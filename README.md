# globus_gridmap_umich_avp_callout

Implements Globus Tooklit grid-to-local user mapping and authorization policies for the Advanced Research Computing Visualization Portal at the University of Michigan.

* An ASN.1 BER encoded eduPersonPrincipalName (ePPN) (oid 1.3.6.1.4.1.5923.1.1.1.6) is extracted from the X509v3 extensions of the user's certificate.  If no ePPN is present, or if the certificate signature cannot be verified, no mapping is performed.
* If the ePPN ends in `@umich.edu`, the portion of the ePPN to the left of the `@` sign is used as the mapped local username.
* Otherwise, all `@` and `.` characters in the ePPN are replaced with `-` and the result is used as the mapped local username.

For example:

* A certificate with an ePPN of `markmont@umich.edu` will map to the local user `markmont`
* A certificate with an ePPN of `mam196@duke.edu` will map to the local user `mam196-duke-edu`

globus_gridmap_umich_avp_callout is a modified version of the globus_gridmap_eppn callout from the `gsi/gridmap_eppn_callout` subdirectory of the Globus Toolkit version 6.0 source code.

## Installation

```
./configure --prefix=/usr --libdir=/usr/lib64 2>&1 | tee log.configure
make 2>&1 | tee log.make

sudo make install 2>&1 | tee log.install

sudo cat >> /etc/grid-security/gsi-authz.conf << __EOF__
globus_mapping /usr/lib64/libglobus_gridmap_umich_avp_callout.so globus_gridmap_umich_avp_callout ENV:GLOBUS_MYPROXY_CA_CERT=/etc/grid-security/certificates/cilogon-basic.pem GLOBUS_MYPROXY_AUTHORIZED_DN="/DC=org/DC=cilogon/C=US/"
__EOF__

sudo service gsisshd restart
```

## License

Copyright 1999-2006 University of Chicago

Modifications Copyright 2015 Regents of the University of Michigan

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

